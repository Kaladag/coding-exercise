const MESSAGE = readline().split('');

let binaryString = [];

for(let i = 0; i < MESSAGE.length; i++) {
    let binaryWord = MESSAGE[i].charCodeAt(0).toString(2);
    binaryString += new Array(8-binaryWord.length).join('0') + binaryWord;
}

let binStrArray = binaryString.split('');

let output = '';
let counter = 0;

let convertBin = (element, counter) => {
    let elQuantity = '';
    element === '1' ? element = '0' : element = '00';
    for(let i = 0; i < counter; i++) {
        elQuantity += '0';
    }
    return `${element} ${elQuantity} `;
};

for (let i = 0; i < binStrArray.length; i++) {

    if(i > 0 && binStrArray[i] !== binStrArray[i-1]) {
        output += convertBin(binStrArray[i-1], counter);
        counter = 1;
    } else {
        counter++;
    }
}

output += convertBin(binStrArray[binStrArray.length-1], counter);

console.log (output.slice(0, -1));


// **************************** other solution: ****************************
// for (let i = 0; i < binStrArray.length; i++) {
//
//     if(i > 0 && binStrArray[i] !== binStrArray[i-1]) {
//         output += convert(binStrArray[i-1], counter);
//         counter = 1;
//     } else {
//         counter++;
//     }
//
// }
// output += convert(binStrArray[binStrArray.length-1], counter);
//
// console.log (output.slice(0, -1));
// console.log (output);


// pomocny vypocet

// let message = 'Hello World!';
// let messageArray = message.split('');
//
// console.log('messageArray length: ' + messageArray.length);
// console.log('messageArray: ' + messageArray);
// let binaryString = [];
//
// for(let i = 0; i < messageArray.length; i++) {
//     let binaryWord = messageArray[i].charCodeAt(0).toString(2);
//     binaryString += new Array(8-binaryWord.length).join('0') + binaryWord;
//     // binaryString.push(new Array(8-binaryWord.length).join('0') + binaryWord);
//     // vysvetleni k ochcavce s new Array vyse:
//     // var a = new Array(1)
//     // a.join('0') // ""
//     // var b = new Array(2)
//     // b.join('0') // "0"
//     // b.join('0')+'123456' // "0123456"
// }
// console.log('binaryString: ' + binaryString);
// console.log('binaryString length: ' + binaryString.length);
//
// let binStrArray = binaryString.split('');
// console.log('binaryString array: ' + binStrArray);
//
// let output = '';
// let counter = 0;
//
// let convert = (element, counter) => {
//     let elQuantity = '';
//     element === '1' ? element = '0' : element = '00';
//     for(let i = 0; i < counter; i++) {
//         elQuantity += '0';
//     }
//     return `${element}*${elQuantity}*`;
// };

// console.log('output: ' + convert('1', 4));
// convert('1', 4);
// console.log(convert); // zobrazi definici fce

// for (let i = 0; i < binStrArray.length; i++) {
//
//     if(i > 0 && binStrArray[i] !== binStrArray[i-1]) {
//         counter = 1;
//     } else {
//         counter++;
//     }
//
//     if (binStrArray[i] !== binStrArray[i-1] && binStrArray[i] === binStrArray[i+1] ||
//         binStrArray[i] === binStrArray[i-1] && binStrArray[i] === binStrArray[i+1])
//         { continue; }
//
//     output += convert(binStrArray[i], counter);
// }

// console.log (output.slice(0, -1));
