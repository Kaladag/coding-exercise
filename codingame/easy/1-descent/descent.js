while (true) {
    let mountains = [];
    for (let i = 0; i < 8; i++) {
        const mountainH = parseInt(readline());
        mountains[i] = mountainH;
    }

    let maxH = Math.max(...mountains);

    console.log(mountains.indexOf(maxH));
}


// other solution
while (true) {
    let mountains = [];
    let maxH;
    for (let i = 0; i < 8; i++) {
        const mountainH = parseInt(readline()); // represents the height of one mountain.
        mountains[i] = mountainH;
        if (i === 0) {
            maxH = mountains[i];
        } else {
            maxH = mountains[i] > maxH ? mountains[i] : maxH;
        }
    }
    console.log(mountains.indexOf(maxH));     // The index of the mountain to fire on.
}

