let inputs = readline().split(' ');
const lightX = parseInt(inputs[0]); // the X position of the light of power
const lightY = parseInt(inputs[1]); // the Y position of the light of power
const initialTx = parseInt(inputs[2]); // Thor's starting X position
const initialTy = parseInt(inputs[3]); // Thor's starting Y position

// **************************** solution ****************************
let moveTx = initialTx;
let moveTy = initialTy;

while (true) {
    const remainingTurns = parseInt(readline());

    let move = '';
    if(moveTy > lightY) {
        move += 'N';
        moveTy--;
    }
    if(moveTy < lightY) {
        move += 'S';
        moveTy++;
    }
    if(moveTx > lightX) {
        move += 'W';
        moveTx--;
    }
    if(moveTx < lightX) {
        move += 'E';
        moveTx++;
    }
    console.log(move);
}

// **************************** other solution: ****************************
let thorX = parseInt(inputs[2]); // Thor's starting X position
let thorY = parseInt(inputs[3]); // Thor's starting Y position

while (true) {
    let remainingTurns = parseInt(readline());

    let direction = "";
    if(thorY > lightY) {
        direction = "N";
        thorY--;
    } else if(thorY < lightY ) {
        direction = "S";
        thorY++;
    }

    if(thorX > lightX) {
        direction += "W";
        thorX--;
    } else if(thorX < lightX ) {
        direction += "E";
        thorX++;
    }

    print(direction); // A single line providing the move to be made: N NE E SE S SW W or NW
}




















