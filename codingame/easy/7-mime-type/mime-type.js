const N = parseInt(readline()); // Number of elements which make up the association table.
const Q = parseInt(readline()); // Number Q of file names to be analyzed.
const inputsTable = new Map();

for (let i = 0; i < N; i++) {
    let inputs = readline().split(' ');
    inputsTable.set(inputs[0].toLowerCase(), inputs[1]);
}

for (let i = 0; i < Q; i++) {
    const FNAME = readline();
    let output = '';
    if(!FNAME.includes('.') || FNAME.endsWith('.')) {
        output = 'none';
    } else {
        output = FNAME.split('.').pop().toLowerCase();
    }

    console.log(inputsTable.get(output) === undefined ? 'UNKNOWN' : inputsTable.get(output));
}


// pomocny vypocet

// let inputs = ['HTML','text/html','png','image/png','GIF','image/gif'];
// const FNAME = ['animated.GIF', 'portrait.png', 'INDEX.html', 'script.JS', '.pdf', 'mp3', 'final.','b.wav.tmp', 'report..pdf'];
// let fnameSplit = [];
//
// for (let i = 0; i < FNAME.length; i++) {
//     if(!FNAME[i].includes('.') || FNAME[i].endsWith('.')) {
//         fnameSplit.push('none');
//     } else {
//         fnameSplit.push(FNAME[i].split(".").pop());
//     }
// }
// console.log('fnameSplit: ' + fnameSplit);
//
// let inputsTable = new Map();
//
// for (let i = 0; i < inputs.length; i += 2) {
//     // const EXT = inputs[i]; // file extension
//     // const MT = inputs[i+1]; // MIME type.
//     inputsTable.set(inputs[i], inputs[i+1]);
// }
//
// for (let [key, value] of inputsTable) {
//     console.log(key + ' = ' + value)
// }
//
// for (let i = 0; i < fnameSplit.length; i++) {
//     if (inputsTable.get(fnameSplit[i]) === undefined) {
//         console.log('UNKNOWN');
//     } else {
//         console.log(inputsTable.get(fnameSplit[i]));
//     }
// }

// inputsTable.forEach((value, key) => {
//     for (let i = 0; i < fnameSplit.length; i++) {
//         if (key === fnameSplit[i]) {
//             console.log(i + ': ' + value);
//         } else {
//             console.log(i + ': UNKNOW');
//         }
//     }
// });
