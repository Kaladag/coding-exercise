const n = parseInt(readline()); // the number of temperatures to analyse
let inputs = readline().split(' '); // a string with the N temperatures expressed as integers ranging from -273 to 5526

// **************************** solution ****************************
let result = 0;

for (let i = 0; i < n; i++) {
    if(result === 0) {
        result = inputs[i];
    } else if (inputs[i] > 0 && inputs[i] <= Math.abs(result)) {
        result = inputs[i];
    } else if (inputs[i] < 0 && -inputs[i] < Math.abs(result)) {
        result = inputs[i];
    }
}

console.log(result);

// pomocny vypocet

// varianty hodnot v poli teplot:
let inputsArray = ['-2', '2', '0', '99', '587', '-698', '-5', '3', '10'];
// let inputsArray = ['-10', '-2', '-1', '-5'];
// let inputsArray = ['10', '2', '5'];
// let inputsArray = [];
// let inputsArray = ['0'];

for (let i = 0; i < inputsArray.length; i++) {
    if(result === 0) {
        result = inputsArray[i];
    } else if (inputsArray[i] > 0 && inputsArray[i] <= Math.abs(result)) {
        result = inputsArray[i];
    } else if (inputsArray[i] < 0 && Math.abs(inputsArray[i]) < Math.abs(result)) {
        result = inputsArray[i];
    }
}

console.log(result);
// Math.abs(inputsArray[i]) lze nahradit: -inputsArray[i]
//console.log('-inputsArray[i]: ' + -inputsArray[1]);


// **************************** other solution ****************************
// let inputs = readline().split(' ').map(Number);
// /* musela jsem prevest na pole cisel, jinak nefungovala podminka pro splneni varianty, ze nejsou k dispozici
// zadne teploty */
//
// let positiveNumbers = [];
// let negativeNumbers = [];
// let result;
//
// for (let i = 0; i < n; i++) {
//     if(inputs[i] > 0) {
//         positiveNumbers.push(inputs[i]);
//     } else if (inputs[i] < 0) {
//         negativeNumbers.push(inputs[i]);
//     }
// }
//
// positiveNumbers.sort((a, b) => a-b);
// negativeNumbers.sort((a, b) => b-a);
//
// if ((!positiveNumbers[0] || !negativeNumbers[0]) && inputs[0] === 0) {
//     result = 0;
// } else if (positiveNumbers[0] <= Math.abs(negativeNumbers[0]) || !negativeNumbers[0]) {
//     result = positiveNumbers[0];
// } else {
//     result = negativeNumbers[0];
// }
//
// console.log(result);

// // pomocny vypocet
//
// // varianty hodnot v poli teplot:
// // let inputsArray = ['-1', '2', '0', '99', '587', '-698', '-5', '3', '10'];
// // let inputsArray = ['-10', '-2', '-5'];
// // let inputsArray = ['10', '2', '5'];
// // let inputsArray = [];
// let inputsArray = ['0']; // v zadani pouzivali stringy, ktere jsme museli prevest na pole cisel; po-te stale hazelo
//         // chyby pri zpracovani podminky na prazdne pole (tedy bez teplot), ptze jim to hazelo, ze pole stale obsahuje
//         // jeden znak/element, tedy inputs.length rovnal se 1, obsahovalo vpodstate: [' ']; timto zadanim jsme otestovali
//         // tuto variantu na vstupu
//
// let positiveNumbers = [];
// let negativeNumbers = [];
// let result;
//
// // podminka umistena v kodu zde byla bezpredmetna, ptze pri nespravne definovanych podminkach nize se promenna result opet prepsala
// // if (inputsArray.length === 0) {
// //     result = 0;
// // }
//
// for (let i = 0; i < inputsArray.length; i++) {
//     if (inputsArray[i] > 0) {
//         positiveNumbers.push(inputsArray[i]);
//     } else if (inputsArray[i] < 0) {
//         negativeNumbers.push(inputsArray[i]);
//     }
// }
//
// // serazeni elementu v polich, abychom mohli pracovat s elementem indexu 0
// positiveNumbers.sort((a, b) => a - b);
// negativeNumbers.sort((a, b) => b - a);
//
// if ((!positiveNumbers[0] || !negativeNumbers[0]) && (inputsArray.length === 0 || inputsArray[0] === '0')) {
//     result = 0;
// } else if (positiveNumbers[0] <= Math.abs(negativeNumbers[0]) || !negativeNumbers[0]) {
//     result = positiveNumbers[0];
// } else {
//     result = negativeNumbers[0];
// }
//
// // else if(!positiveNumbers[0] || positiveNumbers[0] > Math.abs(negativeNumbers[0])) {
// //     result = negativeNumbers[0];
// // }
//
// console.log(result);


