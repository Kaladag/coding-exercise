// instructions
// How Much is True?
//     Create a function which returns the number of true values there are in an array.
//
//     Examples
// countTrue([true, false, false, true, false]) ➞ 2
//
// countTrue([false, false, false, false]) ➞ 0
//
// countTrue([]) ➞ 0

//     Notes
// Return 0 if given an empty array.
//     All array items are of the type bool (true or false).

// solution:
function countTrue (array) {
    let output = [];
    for(let i = 0; i<array.length; i++) {
        if(array[i] === true) {
            output.push(array[i])
        }
    }
    return output.length;
}

console.log(countTrue([true, false, false, true, false]));
console.log(countTrue([false, false, false]));
console.log(countTrue([]));


// other solutions:

// function countTrue (array) {
//     let output = array.filter(item => item === true);
//     return console.log(output.length);
// }


// const countTrue = r => console.log(r.filter(Boolean).length)
// Boolean je constructor v JS, ktery kontroluje zadany parametr, zda je true nebo false (prevadi dle truthy/falsy
// values). Ptze filter metoda vraci do noveho pole pouze true hodnoty, lze zadat vyse uvedenym zpusobem.


















